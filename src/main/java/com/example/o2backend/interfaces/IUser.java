package com.example.o2backend.interfaces;

import com.example.o2backend.models.User;

import java.util.Optional;

public interface IUser {

    public Optional<User> findByUsernameOrEmail(String username);
    public Boolean existsByUsername(String username);
    public Boolean existsByEmail(String email);
    public User save(User user);
}
