package com.example.o2backend.interfaces;

import com.example.o2backend.models.Locataire;

public interface ILocataire {

    public Locataire save(Locataire locataire);
}
