package com.example.o2backend.interfaces;

import com.example.o2backend.models.Hopital;

public interface IHopital {

    public Hopital save(Hopital hopital);
}
