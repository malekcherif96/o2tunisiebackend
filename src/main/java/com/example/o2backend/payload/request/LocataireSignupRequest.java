package com.example.o2backend.payload.request;

import javax.validation.constraints.Email;
import javax.validation.constraints.Size;
import java.util.Set;

public class LocataireSignupRequest {

    @Size(min = 3, max = 20)
    private String username;

    @Size(max = 50)
    @Email
    private String email;

    private Set<String> role;

    @Size(min = 6, max = 40)
    private String password;

    private float prixCinqlitre;
    private float prixDixlitre;
    private String firstName;
    private String lastName;
    private String phoneNumber;
    private String province;
    private String ville;
    private String adresse;
    private int nbConcentrateurs;
    private boolean dixLitres;
    private boolean cinqLitres;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Set<String> getRole() {
        return role;
    }

    public void setRole(Set<String> role) {
        this.role = role;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getVille() {
        return ville;
    }

    public void setVille(String ville) {
        this.ville = ville;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public int getNbConcentrateurs() {
        return nbConcentrateurs;
    }

    public void setNbConcentrateurs(int nbConcentrateurs) {
        this.nbConcentrateurs = nbConcentrateurs;
    }

    public boolean isDixLitres() {
        return dixLitres;
    }

    public void setDixLitres(boolean dixLitres) {
        this.dixLitres = dixLitres;
    }

    public boolean isCinqLitres() {
        return cinqLitres;
    }

    public void setCinqLitres(boolean cinqLitres) {
        this.cinqLitres = cinqLitres;
    }

    public float getPrixCinqlitre() {
        return prixCinqlitre;
    }

    public void setPrixCinqlitre(float prixCinqlitre) {
        this.prixCinqlitre = prixCinqlitre;
    }

    public float getPrixDixlitre() {
        return prixDixlitre;
    }

    public void setPrixDixlitre(float prixDixlitre) {
        this.prixDixlitre = prixDixlitre;
    }
}
