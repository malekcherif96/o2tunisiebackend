package com.example.o2backend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class O2backendApplication {

    public static void main(String[] args) {
        SpringApplication.run(O2backendApplication.class, args);
    }

}
