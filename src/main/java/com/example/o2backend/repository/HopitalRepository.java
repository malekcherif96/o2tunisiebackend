package com.example.o2backend.repository;

import com.example.o2backend.models.Hopital;

import javax.transaction.Transactional;

@Transactional
public interface HopitalRepository extends UserBaseRepository<Hopital>{
}
