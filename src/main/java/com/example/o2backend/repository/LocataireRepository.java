package com.example.o2backend.repository;

import com.example.o2backend.models.Locataire;

public interface LocataireRepository extends UserBaseRepository<Locataire> {
}
