package com.example.o2backend.repository;

import com.example.o2backend.models.User;
import org.springframework.data.jpa.repository.Query;


import javax.transaction.Transactional;
import java.util.Optional;

@Transactional
public interface UserRepository extends UserBaseRepository<User> {
    @Query("select u from User u where u.email = ?1 OR u.username = ?1")
    Optional<User> findByUsernameOrEmail(String username);
    Optional<User> findByUsername(String username);
    Boolean existsByUsername(String username);
    Boolean existsByEmail(String email);
}
