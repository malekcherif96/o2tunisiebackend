package com.example.o2backend.models;

import javax.persistence.Entity;
import java.util.Set;

@Entity
public class Hopital extends User{


    private String nameHopital;
    private String province;
    private String ville;
    private String adresse;
    private String phoneNumber;

    public Hopital() {}


    public Hopital(String username, String email, String encode, String nameHopital, String province, String ville, String adresse, String phoneNumber) {
        super(username, email, encode);
        this.nameHopital = nameHopital;
        this.province = province;
        this.ville = ville;
        this.adresse = adresse;
        this.phoneNumber = phoneNumber;
    }


    public String getNameHopital() {
        return nameHopital;
    }

    public void setNameHopital(String nameHopital) {
        this.nameHopital = nameHopital;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getVille() {
        return ville;
    }

    public void setVille(String ville) {
        this.ville = ville;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
}
