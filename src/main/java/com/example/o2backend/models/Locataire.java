package com.example.o2backend.models;

import javax.persistence.Entity;
import java.util.Set;

@Entity
public class Locataire extends User{

    private String firstName;
    private String lastName;
    private String phoneNumber;
    private String province;
    private String ville;
    private float prixCinqlitre;
    private float prixDixlitre;
    private String adresse;
    private int nbConcentrateurs;
    private boolean dixLitres;
    private boolean cinqLitres;

    public Locataire() {
        super();
    }


    public Locataire(String username, String email, String encode, String firstName, String lastName, String phoneNumber, String province, String ville, String adresse, int nbConcentrateurs, boolean dixLitres, boolean cinqLitres, float prixCinqlitre,float prixDixlitre) {
        super(username, email, encode);
        this.firstName = firstName;
        this.lastName = lastName;
        this.phoneNumber = phoneNumber;
        this.province = province;
        this.ville = ville;
        this.adresse = adresse;
        this.nbConcentrateurs = nbConcentrateurs;
        this.dixLitres = dixLitres;
        this.cinqLitres = cinqLitres;
        this.prixCinqlitre =prixCinqlitre;
        this.prixDixlitre=prixDixlitre;
    }


    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getVille() {
        return ville;
    }

    public void setVille(String ville) {
        this.ville = ville;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public int getNbConcentrateurs() {
        return nbConcentrateurs;
    }

    public void setNbConcentrateurs(int nbConcentrateurs) {
        this.nbConcentrateurs = nbConcentrateurs;
    }

    public boolean isDixLitres() {
        return dixLitres;
    }

    public void setDixLitres(boolean dixLitres) {
        this.dixLitres = dixLitres;
    }

    public boolean isCinqLitres() {
        return cinqLitres;
    }

    public void setCinqLitres(boolean cinqLitres) {
        this.cinqLitres = cinqLitres;
    }

    public float getPrixCinqlitre() {
        return prixCinqlitre;
    }

    public void setPrixCinqlitre(float prixCinqlitre) {
        this.prixCinqlitre = prixCinqlitre;
    }

    public float getPrixDixlitre() {
        return prixDixlitre;
    }

    public void setPrixDixlitre(float prixDixlitre) {
        this.prixDixlitre = prixDixlitre;
    }
}
