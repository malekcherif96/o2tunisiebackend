package com.example.o2backend.controllers;

import com.example.o2backend.models.Hopital;
import com.example.o2backend.models.Locataire;
import com.example.o2backend.payload.request.HopitalSignupRequest;
import com.example.o2backend.payload.request.LocataireSignupRequest;
import com.example.o2backend.services.HopitalService;
import com.example.o2backend.services.LocataireService;
import org.springframework.security.crypto.password.PasswordEncoder;
import com.example.o2backend.models.ERole;
import com.example.o2backend.models.Role;
import com.example.o2backend.payload.request.LoginRequest;
import com.example.o2backend.payload.response.JwtResponse;
import com.example.o2backend.payload.response.MessageResponse;
import com.example.o2backend.repository.RoleRepository;
import com.example.o2backend.repository.UserRepository;
import com.example.o2backend.security.jwt.JwtUtils;
import com.example.o2backend.security.services.UserDetailsImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/auth")
public class AuthController {
    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    UserRepository userRepository;

    @Autowired
    HopitalService hopitalService;

    @Autowired
    LocataireService locataireService;

    @Autowired
    RoleRepository roleRepository;

    @Autowired
    PasswordEncoder encoder;

    @Autowired
    JwtUtils jwtUtils;
    @PostMapping("/signin")
    public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginRequest loginRequest) {

        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(loginRequest.getUsername(), loginRequest.getPassword()));

        SecurityContextHolder.getContext().setAuthentication(authentication);
        String jwt = jwtUtils.generateJwtToken(authentication);

        UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
        List<String> roles = userDetails.getAuthorities().stream()
                .map(item -> item.getAuthority())
                .collect(Collectors.toList());

        return ResponseEntity.ok(new JwtResponse(jwt,
                userDetails.getId(),
                userDetails.getUsername(),
                userDetails.getEmail(),
                roles));
    }

    @PostMapping("/signup/hopital")
    public ResponseEntity<?> registerHopital(@Valid @RequestBody HopitalSignupRequest hopitalSignupRequest) {
        if (userRepository.existsByUsername(hopitalSignupRequest.getUsername())) {
            return ResponseEntity
                    .badRequest()
                    .body(new MessageResponse("Error: Username is already taken!"));
        }

        if (userRepository.existsByEmail(hopitalSignupRequest.getEmail())) {
            return ResponseEntity
                    .badRequest()
                    .body(new MessageResponse("Error: Email is already in use!"));
        }

        // Create new user's account
        Hopital hopital = new Hopital(hopitalSignupRequest.getUsername(),
                hopitalSignupRequest.getEmail(),
                encoder.encode(hopitalSignupRequest.getPassword()),hopitalSignupRequest.getNameHopital()
                ,hopitalSignupRequest.getProvince(),hopitalSignupRequest.getVille(),
                hopitalSignupRequest.getAdresse(),hopitalSignupRequest.getPhoneNumber());
        Set<String> strRoles = hopitalSignupRequest.getRole();
        Set<Role> roles = new HashSet<>();

        Role hospitalRole = roleRepository.findByName(ERole.ROLE_HOSPITAl)
                .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
        roles.add(hospitalRole);

        hopital.setRoles(roles);
        hopitalService.save(hopital);
        return ResponseEntity.ok(new MessageResponse("Hospital registered successfully!"));
    }

    @PostMapping("/signup/locataire")
    public ResponseEntity<?> registerLocataire(@Valid @RequestBody LocataireSignupRequest locataireSignupRequest) {
        if (userRepository.existsByUsername(locataireSignupRequest.getUsername())) {
            return ResponseEntity
                    .badRequest()
                    .body(new MessageResponse("Error: Username is already taken!"));
        }

        if (userRepository.existsByEmail(locataireSignupRequest.getEmail())) {
            return ResponseEntity
                    .badRequest()
                    .body(new MessageResponse("Error: Email is already in use!"));
        }

        // Create new user's account
        Locataire locataire = new Locataire(locataireSignupRequest.getUsername(),
                locataireSignupRequest.getEmail(),
                encoder.encode(locataireSignupRequest.getPassword()),locataireSignupRequest.getFirstName(),
                locataireSignupRequest.getLastName(),locataireSignupRequest.getPhoneNumber(),locataireSignupRequest.getProvince(),
                locataireSignupRequest.getVille(),locataireSignupRequest.getAdresse(),locataireSignupRequest.getNbConcentrateurs(),
                locataireSignupRequest.isDixLitres(),locataireSignupRequest.isCinqLitres(), locataireSignupRequest.getPrixCinqlitre(), locataireSignupRequest.getPrixDixlitre());
        Set<String> strRoles = locataireSignupRequest.getRole();
        Set<Role> roles = new HashSet<>();

        Role locataireRole = roleRepository.findByName(ERole.ROLE_LOCATAIRE)
                .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
        roles.add(locataireRole);

        locataire.setRoles(roles);
        locataireService.save(locataire);
        return ResponseEntity.ok(new MessageResponse("User registered successfully!"));
    }
}
