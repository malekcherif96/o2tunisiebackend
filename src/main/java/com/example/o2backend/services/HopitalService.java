package com.example.o2backend.services;

import com.example.o2backend.interfaces.IHopital;
import com.example.o2backend.models.Hopital;
import com.example.o2backend.repository.HopitalRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class HopitalService implements IHopital {

    @Autowired
    HopitalRepository hopitalRepository;

    @Override
    public Hopital save(Hopital hopital) {
        return hopitalRepository.save(hopital);
    }
}
