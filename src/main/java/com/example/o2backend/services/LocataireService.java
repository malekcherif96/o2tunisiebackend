package com.example.o2backend.services;

import com.example.o2backend.interfaces.ILocataire;
import com.example.o2backend.models.Locataire;
import com.example.o2backend.repository.LocataireRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class LocataireService implements ILocataire {

    @Autowired
    LocataireRepository locataireRepository;

    @Override
    public Locataire save(Locataire locataire) {
        return locataireRepository.save(locataire);
    }
}
